"hgen" is free software.

This program is distributed in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

## hgen

Documentation generator for [D](https://www.dlang.org) with
limited [Markdown](https://daringfireball.net/projects/markdown/) support
and [DDoc](https://dlang.org/spec/ddoc.html), based on **harbored-mod**.

**Official repositories**:

- [https://codeberg.org/os-18/hgen](https://codeberg.org/os-18/hgen)
- [https://gitlab.com/os-18/hgen](https://gitlab.com/os-18/hgen)

## Getting started

### Build from source

You can use *`make`* to build the application or *`dub`* directly
(the project's Makefile uses *`dub`* for assembly).

### Setting up

At this point you should have a binary called **`hgen`** in the *`build/`*
directory.

- Modify your `PATH` to point to the *`build/`* directory or copy the
  binary into any suitable directory.
  Also, you can run `make install` (*`/usr/local/bin`* by default).

- From your project's directory, run **`hgen`**. This assumes your source
  code is in the *`./source`* subdirectory:

    `hgen source`

    This will write documentation to the *`./doc`* directory.


## Features

- Supports DDoc and (most, see differences) Markdown syntax.
- Sensible defaults (get decent documentation without tweaking any settings).
- Automatic cross-referencing in code blocks and inline code.
- Very fast.
- Generates one file per module/class/struct/enum, etc.
- Generated docs are usable without JavaScript (e.g. NoScript),
  JS may used for optional functionality.
- Only generates HTML, and is unlikely to support any other formats.
- All command-line options can be set in the config file *`hgen.cfg`*.


## Differences from vanilla Markdown

Sequence `---` will not generate a horizontal line, as it is used
for DDoc blocks. Use `- - -` instead. This is still standard Markdown.

*Emphasis* can be denoted by `*`, but not by `_` (this would break
snake\_case names).

Also this does not work (again because DDoc uses `---` to mark code blocks):
```
Subheading
----------
```
Instead, use either (standard Markdown):
```
## Subheading
```


## Credits

This project is a fork of
[harbored-mod](https://github.com/dlang-community/harbored-mod)
(maintained by the Dlang community).
Harbored-mod was continued in a
[new repository](https://gitlab.com/basile.b/harbored-mod) by *Basile.B*;
some ideas from this new repository was added on to **hgen**.

**Harbored-mod** is a fork of
[harbored](https://github.com/economicmodeling/harbored)
written by *Brian Schott* and *Ferdinand Majerech aka Kiith-Sa*.

The current developer and maintainer of **hgen** is
*Eugene 'Vindex' Stulin* <tech.vindex@gmail.com>.

This program is distributed under
the [Boost Software License](http://www.boost.org/LICENSE_1_0.txt).
