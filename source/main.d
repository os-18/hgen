/* This file is part of the 'hgen' project.
 *
 * Copyright (c) 2014-2025
 *     Economic Modeling Specialists, Intl.,
 *     the D Community.
 *
 * Main contributors:
 *     Brian Schott
 *     Ferdinand Majerech
 *     Akzhigitov Anton
 *     Nemanja Boric
 *     Basile Burg
 *     Eugene Stulin
 *
 * Distributed under the Boost Software License, Version 1.0.
 *
 * You should have received a copy of the Boost Software License
 * along with this program. If not, see <http://www.boost.org/LICENSE_1_0.txt>.
 * This file is offered as-is, without any warranty.
 */

module main;

import
    std.algorithm,
    std.array,
    std.conv,
    std.datetime,
    std.exception,
    std.file,
    std.getopt,
    std.path,
    std.stdio,
    std.string;

import
    dparse.ast,
    dparse.lexer,
    dparse.parser;

import ddoc.lexer;

import allocator;
import config;
import macros;
import symboldatabase;
import tocbuilder;
import unittest_preprocessor;
import visitor;
import writer;

immutable string defaultConfigPath = "hgen.cfg";
immutable string linuxConfigDir;
immutable string linuxConfigPath;
immutable string language;


shared static this() {
    import std.process : environment;
    version (linux) {
        auto home = environment["HOME"];
        linuxConfigDir = std.path.buildPath(home, ".config", "hgen");
        linuxConfigPath = std.path.buildPath(linuxConfigDir, "hgen.cfg");
    }
    auto LANG = environment.get("LANG", "en_US.UTF-8");
    auto langAndCharset = LANG.split(".").array;
    auto tempLang = "";
    if (langAndCharset.length > 0) {
        auto langAndCountry = langAndCharset[0].split("_").array;
        if (langAndCountry.length > 0) {
            tempLang = langAndCountry[0];
        }
    }
    language = (tempLang == "") ? "en" : tempLang;
}


int main(string[] args) {
    const startTime = Clock.currStdTime;

    Config conf;

    conf.loadConfigFile(getDefaultConfigPath());
    conf.loadCLI(args);

    if (conf.doHelp) {
        writeln(helpTranslations.get(language, "en"));
        return 0;
    }

    if (conf.showVersion) {
        writeln(versionString);
        return 0;
    }

    scope(exit) {
        if (conf.performance) {
            writefln!"Time spent: %.3fs"(
                (Clock.currStdTime - startTime) / 10_000_000.0
            );
            version(linux) {
                writefln("Peak memory usage (KiB): %s", peakMemoryUsageK());
            }
        }
    }

    if (conf.doGenCSSPath !is null) {
        writefln("Generating CSS file '%s'", conf.doGenCSSPath);
        return writeProtected(conf.doGenCSSPath, stylecss, "CSS");
    }
    if (conf.doGenConfig) {
        writefln("Generating conf file '%s'", defaultConfigPath);
        return writeProtected(defaultConfigPath, defaultConfigString, "config");
    }

    version(linux) {
        if (conf.doGenerateConfigForLinux) {
            if (!std.file.exists(linuxConfigPath)) {
                mkdirRecurse(linuxConfigDir);
                writefln("Generating config file '%s'", linuxConfigPath);
                return writeProtected(
                    linuxConfigPath, defaultConfigString, "config"
                );
            }
            return 0;
        }
    }

    try {
        conf.macros = readMacros(conf.macroFileNames);
    } catch (Exception e) {
        stderr.writeln(e.msg);
        return 1;
    }

    generateDocumentation!HTMLWriterAggregated(conf);
    return 0;
}


/*******************************************************************************
 * Returns path to config file by default:
 * in the current directory or (for Linux) in ~/.config/hgen/ if in the current
 * directory the file doesn't exist.
 */
string getDefaultConfigPath() {
    string configPath = defaultConfigPath;
    version (linux) {
        if (!exists(defaultConfigPath) && exists(linuxConfigPath)) {
            configPath = linuxConfigPath;
        }
    }
    return configPath;
}


/// Used to write default CSS/config with overwrite checking.
int writeProtected(string path, string content, string type) {
    if (path.exists) {
        writefln("'%s' exists. Overwrite? (y/N)", path);
        import std.ascii: toLower;
        char overwrite;
        readf("%s", &overwrite);
        if(overwrite.toLower != 'y') {
            writefln("Exited without overwriting '%s'", path);
            return 1;
        }
        writefln("Overwriting '%s'", path);
    }
    try {
        std.file.write(path, content);
    } catch(Exception e) {
        writefln("Failed to write default %s to file `%s` : %s",
            type, path, e.msg);
        return 1;
    }
    return 0;
}


private string[string] readMacros(const string[] macroFiles) {
    import ddoc.macros : defaultMacros = DEFAULT_MACROS;

    string[string] result;
    defaultMacros.byKeyValue.each!(a => result[a.key] = a.value);
    result["D"]    = `<code class="d_inline_code">$0</code>`;
    result["HTTP"] = "<a href=\"http://$1\">$+</a>";
    result["WEB"]  = "$(HTTP $1,$2)";
    uniformCodeStyle(result);
    macroFiles.each!(mf => mf.readMacroFile(result));
    return result;
}


private void uniformCodeStyle(ref string[string] macros) {
    macros[`D_CODE`] = `<pre><code class="hljs_d">$0</code></pre>`;
    macros[`D`] = `<b>$0</b>`;
    macros[`D_INLINECODE`] = `<b>$0</b>`;
    macros[`D_COMMENT`] = `$0`;
    macros[`D_KEYWORD`] = `$0`;
    macros[`D_PARAM`] = macros[`D_INLINECODE`];
}


private void generateDocumentation(Writer)(ref Config conf) {
    string[] files = getFilesToProcess(conf);
    import std.stdio : writeln;
    stdout.writeln("Writing documentation to ", conf.outputDirectory);

    mkdirRecurse(conf.outputDirectory);

    File searchJS = File(buildPath(conf.outputDirectory, "search.js"), "w");
    searchJS.writeln(`"use strict";`);
    searchJS.writeln(`var items = [`);

    auto db = gatherData(conf, new Writer(conf, searchJS, null, null), files);

    TocItem[] tocItems = buildTree(db.moduleNames, db.moduleNameToLink);

    enum noFile = "missing file";
    string[] tocAdditionals =
        conf.tocAdditFileNames.map!(p => p.exists ? readText(p) : noFile).array
        ~ conf.tocAdditStrings;

    if (!tocAdditionals.empty) {
        foreach(ref text; tocAdditionals) {
            auto html = new Writer(conf, searchJS, null, null);
            auto writer = appender!string();
            html.readAndWriteComment(writer, text);
            text = writer.data;
        }
    }

    foreach (f; db.moduleFiles) {
        writeln("Generating documentation for ", f);
        try {
            writeDocumentation!Writer(
                conf, db, f, searchJS, tocItems, tocAdditionals
            );
        } catch (DdocParseException e) {
            stderr.writeln(
                "Could not generate documentation for ",
                f, ": ", e.msg, ": ", e.snippet
            );
        } catch (Exception e) {
            stderr.writeln(
                "Could not generate documentation for ", f, ": ", e.msg
            );
        }
    }
    searchJS.writeln(`];`);
    searchJS.writeln(searchjs);

    // Write index.html and style.css
    {
        writeln("Generating main page");
        File css = File(buildPath(conf.outputDirectory, "style.css"), "w");
        css.write(getCSS(conf.cssFileName));
        File index = File(buildPath(conf.outputDirectory, "index.html"), "w");

        auto fileWriter = index.lockingTextWriter;
        auto html = new Writer(conf, searchJS, tocItems, tocAdditionals);
        html.writeHeader(fileWriter, "Index", 0);
        const projectStr = conf.projectName ~ " " ~ conf.projectVersion;
        const heading = projectStr == " " ? "Main Page"
                                          : (projectStr ~ ": Main Page");
        html.writeBreadcrumbs(fileWriter, heading);
        html.writeTOC(fileWriter);

        // Index content added by the user.
        if (conf.indexFileName !is null) {
            File indexFile = File(conf.indexFileName);
            ubyte[] indexBytes = new ubyte[cast(uint) indexFile.size];
            indexFile.rawRead(indexBytes);
            html.readAndWriteComment(fileWriter, cast(string)indexBytes);
        }

        // A full list of all modules.
        if (db.moduleNames.length <= conf.maxModListLen) {
            html.writeModuleList(fileWriter, db);
        }

        index.writeln(HTML_END);
    }
}


/*******************************************************************************
 * Get the CSS content to write into style.css.
 *
 * If customCSS is not null, try to load from that file.
 */
string getCSS(string customCSS) {
    if (customCSS is null) {
        return stylecss;
    }
    try {
        return readText(customCSS);
    } catch(Exception e) {
        stderr.writefln("Failed to load custom CSS `%s`: %s", customCSS, e.msg);
        return stylecss;
    }
}


/// Creates documentation for the module at the given path
void writeDocumentation(Writer)(
    ref Config config,
    SymbolDatabase database,
    string path,
    File search,
    TocItem[] tocItems,
    string[] tocAdditionals
) {
    LexerConfig lexConfig;
    lexConfig.fileName = path;
    lexConfig.stringBehavior = StringBehavior.source;

    File f = File(path);
    ubyte[] fileBytes = uninitializedArray!(ubyte[])(to!size_t(f.size));
    import core.memory;
    scope(exit) GC.free(fileBytes.ptr);
    f.rawRead(fileBytes);
    StringCache cache = StringCache(1024 * 4);
    auto tokens = getTokensForParser(fileBytes, lexConfig, &cache).array;

    import std.functional : toDelegate;
    import dparse.rollback_allocator;
    RollbackAllocator allocator;

    Module m = parseModule(tokens, path, &allocator, toDelegate(&doNothing));

    TestRange[][size_t] unitTestMapping = getUnittestMap(m);

    auto htmlWriter  = new Writer(config, search, tocItems, tocAdditionals);
    auto visitor = new DocVisitor!Writer(
        config, database, unitTestMapping, fileBytes, htmlWriter
    );
    visitor.visit(m);
}


/*******************************************************************************
 * Get .d/.di files to process.
 *
 * Files that don't exist, are bigger than conf.maxFileSizeK or
 * could not be opened will be ignored.
 *
 * Params:
 *
 * conf = Access to configuration to get source file and directory paths
 *        get max file size.
 *
 * Returns: Paths of files to process.
 */
private string[] getFilesToProcess(ref const Config conf) {
    auto paths = conf.sourcePaths.dup;
    auto files = appender!(string[])();

    void addFile(string path) {
        const size = path.getSize();
        if (size > conf.maxFileSizeK * 1024) {
            writefln(
                "WARNING: '%s' (%skiB) bigger than max file size (%skiB), " ~
                "ignoring", path, size / 1024, conf.maxFileSizeK
            );
            return;
        }
        files.put(path);
    }

    foreach (arg; paths) {
        arg = arg.expandTilde;
        if (!arg.exists) {
            stderr.writefln("WARNING: '%s' does not exist, ignoring", arg);
        } else if (arg.isDir) {
            foreach (fileName; arg.dirEntries("*.{d,di}", SpanMode.depth)) {
                addFile(fileName.expandTilde);
            }
        } else if (arg.isFile) {
            addFile(arg);
        } else {
            stderr.writefln("WARNING: Could not open '%s', ignoring", arg);
        }
    }
    return files.data;
}


/// Special empty function.
void doNothing(string, size_t, size_t, string, bool) {}


/// String representing the default CSS.
immutable string stylecss = import("style.css");
/// String representing the script used to search.
immutable string searchjs = import("search.js");


private long peakMemoryUsageK() {
    version(linux) {
        try {
            string stFile = "/proc/self/status";
            auto l = File(stFile).byLine().filter!(l => l.startsWith("VmHWM"));
            enforce(!l.empty, "No VmHWM in /proc/self/status");
            return l.front.split()[1].to!long;
        } catch(Exception e) {
            stderr.writeln("Failed to get peak memory usage: ", e);
            return -1;
        }
    } else {
        auto msg = "peakMemoryUsageK not implemented on non-Linux platforms";
        stderr.writeln(msg);
        return -1;
    }
}
