/* This file is part of the 'hgen' project.
 *
 * Copyright (c) 2014-2025
 *     Economic Modeling Specialists, Intl.,
 *     the D Community.
 *
 * Main contributors:
 *     Ferdinand Majerech
 *     Eugene Stulin
 *
 * Distributed under the Boost Software License, Version 1.0.
 *
 * You should have received a copy of the Boost Software License
 * along with this program. If not, see <http://www.boost.org/LICENSE_1_0.txt>.
 * This file is offered as-is, without any warranty.
 */

/// Config loading and writing.
module config;


import std.algorithm;
import std.array;
import std.conv: to;
import std.stdio;
import std.string;


/*******************************************************************************
 * Stores configuration data loaded from command-line or config files.
 *
 * Note that multiple calls to loadCLI/loadConfigFile are supported;
 * data loaded with earlier calls is overwritten by later calls
 * (e.g. command-line overriding config file),
 * except arrays like macroFileNames/excludes/sourcePaths:
 * successive calls always add to these arrays instead of overwriting them,
 * so e.g. extra modules can be excluded with command-line.
 */
struct Config {
    bool doHelp = false;
    bool showVersion = false;
    bool performance = false;
    bool doGenConfig = false;
    bool doGenerateConfigForLinux = false;
    string doGenCSSPath = null;
    string[] macroFileNames = [];
    string indexFileName = null;
    string[] tocAdditFileNames = [];
    string[] tocAdditStrings = [];
    string cssFileName = null;
    string outputDirectory = "./doc";
    string projectName = null;
    bool noMarkdown = false;
    string projectVersion = null;
    uint maxFileSizeK = 16_384;
    uint maxModListLen = 256;
    /// Names of packages and modules to exclude from generated documentation.
    string[] excludes = [];
    string[] sourcePaths = [];

    /// Loaded from macroFileNames + default macros;
    /// not set on the command-line.
    string[string] macros;

    /***************************************************************************
     * Load config options from CLI arguments.
     *
     * Params:
     * cliArgs = Command-line args.
     */
    void loadCLI(string[] cliArgs) {
        import std.getopt;

        // If the user requests a config file,
        // we must look for that option  first and process it
        // before other options so the config file doesn't override CLI options
        // (it would override them if loaded after processing the CLI options).
        string configFile;
        string[] newMacroFiles;
        string[] newExcludes;
        try {
            // -h/--help will not pass through due
            // to the autogenerated help option
            auto firstResult = getopt(
                cliArgs,
                std.getopt.config.caseSensitive,
                std.getopt.config.passThrough,
                "config|F",
                &configFile
            );
            doHelp = firstResult.helpWanted;
            if (configFile !is null) {
                loadConfigFile(configFile, true);
            }

            auto getoptResult = getopt(
                cliArgs, std.getopt.config.caseSensitive,
                "css|c",                    &cssFileName,
                "generate-css|C",           &doGenCSSPath,
                "exclude|e",                &newExcludes,
                "generate-cfg|g",           &doGenConfig,
                "generate-cfg-linux",       &doGenerateConfigForLinux,
                "index|i",                  &indexFileName,
                "macros|m",                 &newMacroFiles,
                "max-file-size|M",          &maxFileSizeK,
                "output-directory|o",       &outputDirectory,
                "project-name|p",           &projectName,
                "project-version|n",        &projectVersion,
                "no-markdown|D",            &noMarkdown,
                "toc-additional|t",         &tocAdditFileNames,
                "toc-additional-direct|T",  &tocAdditStrings,
                "max-module-list-length|l", &maxModListLen,
                "version",                  &showVersion,
                "performance",              &performance
            );
        } catch (Exception e) {
            writeln("Failed to parse command-line arguments: ", e.msg);
            writeln("Maybe try 'hgen -h' for help information?");
            return;
        }

        macroFileNames ~= newMacroFiles;
        excludes       ~= newExcludes;
        sourcePaths    ~= cliArgs[1 .. $];
    }

    /***************************************************************************
     * Load specified config file and add loaded data to the configuration.
     *
     * Params:
     *
     * fileName        = Name of the config file.
     * requestedByUser = If true, this is not the default config file and
     *                   has been explicitly requested by the user, i.e. we have
     *                   to inform the user if the file was not found.
     *
     */
    void loadConfigFile(string fileName, bool requestedByUser = false) {
        import std.file: exists, isFile;
        import std.typecons: tuple;

        if (!fileName.exists || !fileName.isFile) {
            if (requestedByUser) {
                writefln("Config file '%s' not found", fileName);
            }
            return;
        }

        writefln("Loading config file '%s'", fileName);
        try {
            auto keyValues =
                File(fileName)
                .byLine
                .map!(l => l.until!(c => ";#".canFind(c)))
                .map!array
                .map!strip
                .filter!(s => !s.empty && s.canFind("="))
                .map!(l => l.findSplit("="))
                .map!(p => tuple(p[0].strip.to!string, p[2].strip.to!string))
                .filter!(p => !p[0].empty);

            foreach (key, value; keyValues) {
                processConfigValue(key, value);
            }
        } catch(Exception e) {
            writefln("Failed to parse config file '%s': %s", fileName, e.msg);
        }
    }

private:
    void processConfigValue(string key, string value) {
        // ensures something like "macros = " won't add an empty string value
        void add(ref string[] array, string value) {
            if (!value.empty) {
                array ~= value;
            }
        }

        switch(key) {
            case "help":                   doHelp = value.to!bool;        break;
            case "generate-cfg":           doGenConfig = value.to!bool;   break;
            case "generate-css":           doGenCSSPath = value;     break;
            case "macros":                 add(macroFileNames, value);    break;
            case "max-file-size":          maxFileSizeK = value.to!uint;  break;
            case "max-module-list-length": maxModListLen = value.to!uint; break;
            case "project-name":           projectName = value;           break;
            case "project-version":        projectVersion = value;        break;
            case "no-markdown":            noMarkdown = value.to!bool;    break;
            case "index":                  indexFileName = value;         break;
            case "toc-additional":         add(tocAdditFileNames, value); break;
            case "toc-additional-direct":  add(tocAdditStrings, value);   break;
            case "css":                    cssFileName = value;           break;
            case "output-directory":       outputDirectory = value;       break;
            case "exclude":                add(excludes, value);          break;
            case "config":      if (value) loadConfigFile(value, true);   break;
            case "source":                 add(sourcePaths, value);       break;
            default:
                writefln("Unknown key in config file: '%s'", key);
        }
    }
}


immutable string[string] helpTranslations;
shared static this() {
    helpTranslations["en"] = import("help").strip;
    helpTranslations["ru"] = import("help_ru").strip;
}

immutable string versionString = import("version").strip;
immutable string defaultConfigString = import("hgen.cfg");
