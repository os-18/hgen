/*******************************************************************************
 * Copyright: © 2014 Economic Modeling Specialists, Intl.
 * Author: Brian Schott
 * License: $(LINK2 http://www.boost.org/LICENSE_1_0.txt Boost License 1.0)
 *
 * Forked and modified in 2021 for hgen by Eugene 'Vindex' Stulin.
 * The 'hgen' project: https://gitlab.com/os-18/hgen
 * Author: Eugene 'Vindex' Stulin <tech.vindex@gmail.com>
 * License: $(LINK2 http://www.boost.org/LICENSE_1_0.txt Boost License 1.0)
 */

module ddoc.comments;

import std.algorithm : among, each, find, startsWith, endsWith;
import std.exception : enforce;
import std.string : strip, toLower;

import ddoc.sections;
import ddoc.lexer;
import ddoc.lexer : ParseExc = DdocParseException;
import ddoc.highlight : highlight;
import ddoc.macros : expand, KeyValuePair, parseKeyValuePair;


/*******************************************************************************
 * Converts D comment test to Comment structure object.
 *
 * Params:
 *     text = doc string
 *     macros = associative array storing the names and values of macros
 *     removeUnknown = whether to remove unknown macros
 */
Comment parseComment(string text,
                     string[string] macros = null,
                     bool removeUnknown = true)
out(retVal) {
    assert(retVal.sections.length >= 2);
}
do {
    auto sections = splitSections(text);
    string[string] sMacros = macros.dup;
    auto m = sections.find!(p => p.name == "Macros");
    const e = sections.find!(p => p.name == "Escapes");
    auto p = sections.find!(p => p.name == "Params");
    if (m.length) {
        enforce(
            doMapping(m[0]),
            new ParseExc("Unable to parse Key/Value pairs", m[0].content)
        );
        foreach (kv; m[0].mapping) {
            sMacros[kv[0]] = kv[1];
        }
    }
    if (e.length) {
        assert(0, "Escapes not handled yet");
    }
    if (p.length) {
        enforce(
            doMapping(p[0]), // p[0] - we can have one 'params' section only
            new ParseExc("Unable to parse Key/Value pairs", p[0].content)
        );
        foreach (ref kv; p[0].mapping) {
            kv[1] = expand(Lexer(highlight(kv[1])), sMacros, removeUnknown);
        }
    }

    foreach (ref Section s; sections) {
        if (among(s.name, "Macros", "Escapes", "Params")) {
            continue;
        }
        // without highlight() code blocks works
        // auto h = highlight(s.content);
        // s.content = expand(Lexer(h), sMacros, removeUnknown);
        s.content = expand(Lexer(s.content), sMacros, removeUnknown);
    }

    return Comment(sections);
}


/*******************************************************************************
 * Structure containing a set of doc comment sections.
 */
struct Comment {
    /// Array of comment sections.
    Section[] sections;

    /// Whether the comment refer to the previous one by the phrase "ditto".
    bool isDitto() const @property {
        if (sections.length != 2) {
            return false;
        }
        return sections[0].content.strip().toLower() == "ditto";
    }
}


/// Matches parameter names to their descriptions
private bool doMapping(ref Section s) {
    auto lex = Lexer(s.content);
    KeyValuePair[] pairs;
    if (!parseKeyValuePair(lex, pairs)) {
        return false;
    }
    foreach (kv; pairs) {
        s.mapping ~= kv;
    }
    return true;
}


unittest {
    Comment test = parseComment("Description\nParams:\n    x = thing\n");
    assert(test.sections.length == 3);
    assert(test.sections[0].name == "");
    assert(test.sections[0].content == "Description");
    assert(test.sections[2].name == "Params");
    assert(test.sections[2].content == "x = thing");
    assert(test.sections[2].mapping[0][0] == "x");
    assert(test.sections[2].mapping[0][1] == "thing");
}


unittest {
    auto macros = ["A" : "<a href=\"$0\">"];
    auto comment = `Best-comment-ever © 2014

I thought the same. I was considering writing it, actually.
Imagine how having the $(A tool) would have influenced the "final by
default" discussion. Amongst others, of course.

It essentially comes down to persistent compiler-as-a-library
issue. Tools like dscanner can help with some of more simple
transition cases but anything more complicated is likely to
require full semantic analysis.
Params:
    a = $(A param)
Returns:
    nothing of consequence
`;

    Comment c = parseComment(comment, macros);
    import std.string : format;

    assert(c.sections.length == 4);

    assert(c.sections[0].name is null);
    assert(c.sections[0].content == "Best-comment-ever © 2014");

    assert(c.sections[1].name is null);
    assert(c.sections[1].content.startsWith("I thought the same"));
    assert(c.sections[1].content.endsWith("full semantic analysis."));

    assert(c.sections[2].name == "Params");
    assert(c.sections[2].mapping[0][0] == "a");
    assert(c.sections[2].mapping[0][1] == `<a href="param">`);

    assert(c.sections[3].name == "Returns");
    assert(c.sections[3].content == "nothing of consequence");
}


unittest {
    auto macros = ["A" : "<a href=\"$0\">"];
    auto text = `Best $(Unknown comment) ever`;

    Comment c = parseComment(text, macros, true);
    assert(c.sections.length >= 1);
    assert(c.sections[0].name is null);
    assert(c.sections[0].content == "Best  ever");

    c = parseComment(text, macros, false);
    assert(c.sections.length >= 1);
    assert(c.sections[0].name is null);
    assert(c.sections[0].content == "Best $(Unknown comment) ever");
}


unittest {
    auto comment = `---
auto subcube(T...)(T values);
---
Creates a new cube in a similar way to whereCube, but allows the user to
define a new root for specific dimensions.`c;
    string[string] macros;
    const Comment c = parseComment(comment, macros);
}


///
unittest {
    import std.conv : text;

    auto s1 = `Stop the world

This function tells the Master to stop the world, taking effect immediately.

Params:
reason = Explanation to give to the $(B Master)
duration = Time for which the world $(UNUSED)would be stopped
           (as time itself stop, this is always $(F double.infinity))

---
void main() {
  import std.datetime : msecs;
  import master.universe.control;
  stopTheWorld("Too fast", 42.msecs);
  assert(0); // Will never be reached.
}
---

Returns:
Nothing, because nobody can restart it.

Macros:
F= $0`;

    immutable expectedExamples = `<pre class="d_code">`
    ~ "<font color=blue>void</font> main() {\n"
    ~ "  <font color=blue>import</font> std.datetime : msecs;\n"
    ~ "  <font color=blue>import</font> master.universe.control;\n"
    ~ "  stopTheWorld(<font color=red>\"Too fast\"</font>, 42.msecs);\n"
    ~ "  <font color=blue>assert</font>(0); "
    ~ "<font color=green>// Will never be reached.</font>\n"
    ~ "}</pre>";

    auto c = parseComment(s1, null);

    assert(c.sections.length == 6);
    assert(c.sections[0].name is null);
    assert(c.sections[0].content == "Stop the world");

    assert(c.sections[1].name is null);
    const fnDescr = `This function tells the Master to stop the world, `
                  ~ `taking effect immediately.`;
    assert(c.sections[1].content == fnDescr);

    auto s2 = c.sections[2];

    assert(s2.name == "Params");
    assert(s2.mapping[0][0] == "reason");
    assert(s2.mapping[0][1] == "Explanation to give to the <b>Master</b>");
    assert(s2.mapping[1][0] == "duration");
    const durDescr = "Time for which the world would be stopped\n           "
                   ~ "(as time itself stop, this is always double.infinity)";
    assert(s2.mapping[1][1] == durDescr);

    assert(c.sections[3].name == "Examples");
    assert(c.sections[3].content == expectedExamples);

    assert(c.sections[4].name == "Returns");
    assert(c.sections[4].content == "Nothing, because nobody can restart it.");

    assert(c.sections[5].name == "Macros");
    assert(c.sections[5].mapping[0][0] == "F");
    assert(c.sections[5].mapping[0][1] == "$0");
}


unittest {
    import std.stdio : writeln, writefln;

    auto comment = `Unrolled Linked List.

Nodes are (by default) sized to fit within a 64-byte cache line. The number
of items stored per node can be read from the $(B nodeCapacity) field.
See_also: $(LINK http://en.wikipedia.org/wiki/Unrolled_linked_list)
Params:
    T = the element type
    supportGC = true to ensure that the GC scans the nodes of the unrolled
        list, false if you are sure that no references to GC-managed memory
        will be stored in this container.
    cacheLineSize = Nodes will be sized to fit within this number of bytes.`;

    auto parsed = parseComment(comment, null);
    assert(parsed.sections[3].name == "Params");
    assert(parsed.sections[3].mapping.length == 3);
    assert(parsed.sections[3].mapping[0][0] == "T");
    assert(parsed.sections[3].mapping[0][1] == "the element type");
    assert(parsed.sections[3].mapping[1][0] == "supportGC");
    assert(parsed.sections[3].mapping[1][1].startsWith("true to ensure"));
    assert(parsed.sections[3].mapping[2][0] == "cacheLineSize");
    assert(parsed.sections[3].mapping[2][1].startsWith("Nodes will be sized"));
}
