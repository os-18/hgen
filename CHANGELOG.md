# Changelog

---

## [0.5.5] - 2025-02-28

- fixed code blocks view in markdown elements;
- function list can be displayed in multiple columns (1, 2 or 3);
- other visual improvements.

---

## [0.5.4] - 2025-01-04

- additional CSS changes;
- the "--format" option has been removed; the "html-simple" mode is
  no longer available.

---

## [0.5.3] - 2024-12-31

- improving the appearance of generated pages;
- fixed bug: the "export" attribute is valid now.

---

## [0.5.2] - 2022-07-07

- fix empty return type of 'auto' functions;
- change style of result pages.

---

## [0.5.1] - 2021-06-08

- problem of incomplete documentation generation has been resolved;
- small internal changes.

---

## [0.5] - 2021-02-01

- libddoc and dmarkdown was included to the project;
- unneeded show_hide.js was removed from the project;
- added russian version of the help;
- option "--performance" prints the program work time and peak memory usage;
- small changes in generated documentaion style.

---

## [0.4] - 2021-01-23

- сhanged the display style of many elements;
- the build system was modified;
- a lot of small changes to the code was made.
